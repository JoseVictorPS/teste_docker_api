import docker
import time
import database
import readCGroup
import unitChange

'''
@author: José Victor de Paiva e Silva
@started: 10/10/2019
This script runs a container with a memory hard limit, and tries to
increase it's hard limit, once spent an amount of time using 80% of
said limit. It also tries to decrease memory, once the container 
operates under 15M of it's hard limit for a period of time, then
puts the container's logs and image in a database '''

def start():
    #creates a client to comunicate with docker daemon
    client = docker.from_env()

    #Initial memory given to a container(minimum memory limit docker accepts)
    mem_given = 4

    #Unit of memory given
    mem_given_unit = "m"

    #Cast memory given to string as MegaBytes
    mem_str = str(mem_given) + mem_given_unit

    #The image the container is created from
    container_image = "josevictorps/membound:v2"

    #Run the container on the background with a memory limit and MainMemory+Swap limit
    container = client.containers.run(image=container_image,\
                                      detach=True, mem_limit=mem_str, memswap_limit="2g", name="memory_bound")

    #Considers the container starts with enough memory
    enough_memory_time = time.time()

    #Memory to scale up
    mem_scale_up = 2

    #Memory to scale down
    mem_scale_down = 2

    #DEBUGGER
    time_debug = time.time()

    #While the container is up
    while container.status != 'exited':
        #Updates the container instance
        container.reload()
        #If the container exited right after the reload
        if(container.status == 'exited'):
            break
        #Read cgroup metric from the memory.stat file
        mem_swap, major_fault, page_in, rss, cache, page_fault = readCGroup.read_from_file(container)
        #Memory given unit change to bytes
        mem_given_bytes = unitChange.change_to_bytes(mem_given, mem_given_unit)

        #DEBUGGER
        time_to_print = time.time()
        if(time_to_print-time_debug > 10):
            print("\nmem_swap:"+str(mem_swap)+"\nmajor_fault:"+str(major_fault)+"\npage_in:"+str(page_in)+
                  "\nrss:"+str(rss)+"\ncache:"+str(cache)+"\npage_fault:"+str(page_fault)+
                  "\nmem_given_bytes:"+str(mem_given_bytes)+"\n\n\n")
            time_debug = time.time()

        #If the memory usage in main memory is greater than 90% of the memory given
        if(rss + cache > 0.9*mem_given_bytes):
            #DEBUGGER
            print("RSS+CHACE!\n")
            #If the container is using a minimum significant amount of swap
            if mem_swap > 10000:
                # DEBUGGER
                print("SWAP!\n")
                #If container is thrashing memory pages
                if major_fault > 0.50*page_in:
                    # DEBUGGER
                    print("PAGE_FAULT!\n")
                    # Time when container is under pressure
                    pressure_time = time.time()
                    #If the container spent 5 uninterrupted seconds under memory pressure
                    if(pressure_time - enough_memory_time > 5):
                        #Increases memory given by a scalable amount
                        mem_given += mem_scale_up
                        #Cast memory given to string to parse as argument as MegaBytes
                        mem_str = str(mem_given) + mem_given_unit
                        #Updates the memory scale
                        mem_scale_up += 2
                        #Updates the memory hard limit
                        container.update(mem_limit=mem_str)
                        #Upates enough memory time, considering that now the container has memory to proceed for a few seconds
                        enough_memory_time = time.time()
                else:
                    # Time when container is not under pressure
                    enough_memory_time = time.time()

    #Creates a database object to operate the database
    data_connection = database.Database()

    #Insert the container image and logs to the database
    data_connection.insert_history(container_image, container.logs())

    #Show execution history from the database
    history = data_connection.show_history()

    #Prints all rows from the execution history table
    for row in history:
        print(row)

    #Removes the container
    container.remove()


start()