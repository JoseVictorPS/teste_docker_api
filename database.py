import mysql.connector

''' This file holds the class Database
The class Database is responsible for all
operations related to the database '''

class Database:
    #Establishes a connection and a session with the MySQL server
    database = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="password",
        database="container_history"
    )

    #Creates cursor to execute commands
    cursor = database.cursor()

    def insert_history(self, logs=str, image=str):
        #The SQL Command
        sql_command = "INSERT INTO execution VALUES (%s, %s)"
        #The SQL insert arguments
        arguments = (logs, image)
        #Inserts container history into table
        self.cursor.execute(sql_command, arguments)

    def show_history(self):
        #Selects all rows from the execution table
        self.cursor.execute("SELECT * FROM execution")
        #Fetches all rows from the result table
        return self.cursor.fetchall()

    def clear_history(self):
        #Executes command to truncate table execution
        self.cursor.execute("TRUNCATE TABLE execution")


