
'''This file holds functions to
change the units read by the monitor
to bytes, so it can be compared to other
metrics'''

def change_to_bytes(value, unit):
    if unit == 'k':
        value = value * 1000
        return value
    elif unit == 'm':
        value = value * 1000000
        return value
    elif unit == 'g':
        value = value * 1000000000
        return value
