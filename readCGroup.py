import resource

'''
This file holds functions to help read
from the cgroup file to get the docker
overral metrics'''


'''
Function that reads the amount of swap,
major page fault, page in, rss, cache and page fault
then returns these values in bytes'''
def read_from_file(container):
    #Gets the container ID
    id = container.id

    #Path to find the cgroups metrics for the container in the host system
    cgroup_path = '/sys/fs/cgroup/memory/docker/{}/memory.stat'.format(id)

    #Initializes all variables to return with error value
    swap_mem_bytes = major_fault_bytes = page_in_bytes = \
        rss_bytes = cache_bytes = page_fault_bytes = -1

    try:
        #Opens the file where the cgroup metrics are
        with open(cgroup_path, "r") as memory_file:
            #Read all lines from the cgroup memory file
            all_lines = memory_file.readlines()

            #Read swap memory line from the cgroup file
            swap_mem_line = all_lines[7]

            #Read page major fault line from the cgroup file
            major_fault_line = all_lines[11]

            #Get page size in bytes from the host system
            pagesize = resource.getpagesize()

            #Read page in line from cgroup file
            page_in_line = all_lines[8]

            #Read rss line from cgroup file
            rss_line = all_lines[1]

            #Read cache line from cgroup file
            cache_line = all_lines[0]

            #Read page fault line from cgroup file
            page_fault_line = all_lines[10]

            #Substring of the page fault line containing only number of events
            page_fault_n = page_fault_line[8:]

            #Substring of the rss line containing only the bytes part
            rss_bytes = rss_line[4:]

            # Substring of the cache line containing only the bytes part
            cache_bytes = cache_line[6:]

            #Substring of the swap line containing only the bytes part
            swap_mem_bytes = swap_mem_line[5:]

            #Substring of the major fault line containing only the number of events
            major_fault_n = major_fault_line[11:]

            #Substring of the page in line containing only the number of events
            page_in_n = page_in_line[7:]

            #Number of major faults events times size of the page in bytes
            major_fault_bytes = int(major_fault_n) * pagesize

            #Number of page in events times size of the page in bytes
            page_in_bytes = int(page_in_n) * pagesize

            #Number of page faults events times size of the page in bytes
            page_fault_bytes = int(page_fault_n) * pagesize
    finally:
        #Close file stream
        memory_file.close()

        #Returns swap, major_fault, page_in, rss and cache
        return int(swap_mem_bytes), major_fault_bytes, page_in_bytes,\
               int(rss_bytes), int(cache_bytes), page_fault_bytes
